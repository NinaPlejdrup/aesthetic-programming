let maxLevel = 10;
let split = 2;
let level = 0;
let palette = ["#F7717D", "#F7717D", "#DE639A", "#7F2982", "#16001E", "#729EA1", "#B5BD89", "#C8CDA7", "#D2D6B6", "#E7E9D8"];

function setup() {
  createCanvas(windowWidth, windowHeight);
}

function draw() {
  background(250, 250, 245);
  frameRate(1);
  noStroke();

fill(palette[0]);
ellipse(windowWidth - 100, 60, 100);
fill(250, 250, 245);
ellipse(windowWidth - 140, 60, 100);
fill(palette[0]);
rect(0, windowHeight/2 + 220, width);

//tree trunk
translate(windowWidth/2, windowHeight/2 + 220);
rotate(PI);
drawBranch();
  //noLoop();
}

function drawBranch()
{
  //variables instantiate random parameters of branches
  //angle to be split at
  //let angle = PI/4;
  let angle = 1*random(0.8,1.2);
  //length of branches
  let len = 160/(1.3 * level + 1)*random(0.7, 1.3);
  //thickness of branches
  let thickness = 18 / (2 * level + 1);

  //the stroke color is picked from the palette array based on branch level
  stroke(palette[level]);
  //branch thickness is inserted as stroke weight
  strokeWeight(thickness);

  push();
  //if it is not the main branch, rotate the branch a bit
  if(level != 0) rotate(-0.5 * (split - 1) * angle);

  //current branch is drawn
  line(0,0,0, len);

//drawing leaves at only levels 6 through maxLevel - 1
  if(level > 5)
  {
    //1% chance of drawing "flowers"
    if (random(1) < 0.01)
    {

      noStroke();
      for (let i = 0; i < 10; i ++) {
        fill(palette[5]);
        ellipse(0, len-20, 5, 10);
        rotate(PI/5);
        fill(palette[2]);
        ellipse(0, len-20, 5, 10);
        rotate(PI/5);
    }
  }
  //1% chance of drawing "oranges"
  else if (random(1) > 0.01 && random(1) < 0.02)
  {
      noStroke();
      fill(palette[1]);
      ellipse(0, len-10, 15);
}
//remaining 98% chance of drawing leaves
    else {
      noStroke();
      fill(palette[level]);
      //draw leaf-like ellipses
      ellipse(0, len, level*1.5, level*3);
      //move position to the end of drawn branch
    }

  }

  translate(0, len);

  if(level < maxLevel - 1) {
    //if level is less than highest level, increase the level
    level++;
    for(let i = 0; i < split; i++){
      //continue drawing branches and rotating them (note that drawBranch is called inside itself)
      drawBranch();
      rotate(angle);
    }
    level--;
  }
  pop();
}

function windowResized()
{
  //reassigns windowWidth and windowHeight upon resizing the canvas
  resizeCanvas(windowWidth, windowHeight);
}
