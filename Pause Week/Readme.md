# Pause Week
![Screenshot](/Pause%20Week/screenshot.png)

[Try it!](https://ninaplejdrup.gitlab.io/aesthetic-programming/Pause%20Week/)

An animated clock I have made during the pause week.
