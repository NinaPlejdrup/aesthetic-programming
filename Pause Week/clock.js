//declaring font variable
let clockFont;

//black and white colors are instantiated as variables that switch upon toggling day/night mode
let color1 = 0;
let color2 = 255;
//color3 is a grey color that switches between 200 and 55 for aesthetics
let color3 = 200;
//night mode is instantiated as false
let isNightMode = false;

function setup() {
 //create a drawing canvas at window size
 createCanvas(windowWidth, windowHeight);
 //setting angle mode to degrees
 angleMode(DEGREES);
 //loading custom font
clockFont = loadFont('digital-7.ttf');

//button that toggles between day/night mode
button = createButton('Switch mode');
button.position(610, 320);
button.mousePressed(switchMode);
button.style('font-size', '20px');
button.style('padding', '10px');
button.style('background-color', '#000000');
button.style('color', '#ffffff');
}

//function that toggles between day and night mode
function switchMode()
{
  if(isNightMode)
  {
    isNightMode = false
    color1 = 0;
    color2 = 255;
    color3 = 200;
  }
  else if(!isNightMode)
  {
    isNightMode = true;
    color1 = 255;
    color2 = 0;
    color3 = 55;
  }
}

function draw() {

  console.log(isNightMode);

  //background is black
  background(0);

//origin point of everything drawn is set
  translate(300, 300);
  rotate(-90);

//declaration of hour, minute and second variables
  let hr = hour();
  let min = minute();
  let sec = second();

//values of variables are mapped onto the degrees of a circle
  let secHand = map(sec, 0, 60, 0, 360);
  let minHand = map(min, 0, 60, 0, 360);
  let hrHand = map(hr % 12, 0, 12, 0, 360);

//the clock backgrop consists of two ellipses layered on top of each other at the 300, 300 translate point
  fill(color1);
  stroke(color2);
  strokeWeight(3);
  ellipse(0, 0, 470);

  fill(color2);
  noStroke();
  ellipse(0, 0, 450);


//grey minute lines dividing the clock into 60
  for (let i = 0; i < 360; i++)
  {
    rotate(6);
    stroke(color3);
    line(200, 0, 215, 0);
  }

//larger black hour lines dividing the clock into 12
  for (let i = 0; i < 360; i++)
  {
    rotate(30);
    stroke(color1);
    line(190, 0, 215, 0);
  }

//minute hand
  push();
  rotate(minHand);
  stroke(color1);
  strokeCap(ROUND);
  strokeWeight(9);
  line(0, 0, 200, 0);
  stroke(color2);
  strokeWeight(3);
  line(170, 0, 195, 0);
  pop();

//hour hand
  push();
  rotate(hrHand);
  stroke(color1);
  strokeCap(ROUND);
  strokeWeight(9);
  line(0, 0, 160, 0);
  stroke(color2);
  strokeWeight(3);
  line(140, 0, 155, 0);
  pop();

  //second hand
    push();
    rotate(secHand);
    stroke(121, 175, 224);
    strokeCap(SQUARE);
    strokeWeight(4);
    line(-20, 0, 210, 0);
    pop();

//center point consists of two circles
  noStroke();
  fill(121, 175, 224);;
  ellipse(0, 0, 20, 20);
  fill(color2);
  ellipse(0, 0, 15, 15);


//digital clock backdrop
push();
rotate(90);

//digtial clock text displaying current time
fill(121, 175, 224);
noStroke();
textAlign(CENTER);
textSize(65);
textFont(clockFont);

//toLocaleString found on Stackoverflow formats single digit numbers to start with 0 (e.g. 05 instead of 5)
text(hour().toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping:false})
+ ':' + minute().toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping:false})
+ ':' + second().toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping:false}), 380, -10);
pop();
}

//updates canvas width and height
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
