//  search bar section, including input box and button
const inputWrapper = document.querySelector(".input-wrapper");
// search input field
const inputBox = inputWrapper.querySelector("input");
// location input field
const locationInput = document.querySelector(".location-input");

// dropdown menu with grocery suggestions
const suggBox = inputWrapper.querySelector(".autocom-box");

//clickable stores
const storeItems = document.querySelectorAll(".store-items");
//name of store
const storeName = document.querySelectorAll(".storename");
//adress of store
const storeAdress = document.querySelectorAll(".storeadress");

// array for storing objects in the shopping list
let shoppingCart = [];

// html ul element for displaying the contents of the shopping cart to the UI
const list = document.querySelector(".container ul");

// BUTTONS
// search button
const btnSearch = inputWrapper.querySelector(".btnSearch");
// location button
const btnLocation = document.getElementById("location-button")
// navigation buttons
const btnToGroceryList = document.querySelector(".button-grocerylist");
const btnToStoreSelection = document.querySelector(".button-storeselect");
const btnBackToStoreSelection = document.querySelector(".button-backtostoreselect");
const btnToStorePathway = document.querySelector(".button-pathway");

// FILTER
// filter button
const btnFilter = document.querySelector(".button-filter")
// filter popup box
const filterElements = document.querySelector(".filter")
//slider for filtering distrance from user to store
let slider = document.querySelector(".slider");
// html element for displaying the selected value of the slider to the UI
let output = document.querySelector(".slider-value");

//string storing the selected store
let selectedStore = "";
//HTML element for displaying the selectedStore string to the UI
let selectedStoreText = document.querySelector(".selected-store");


// FUNCTIONS

function showSuggestions(items) {
    let listData;
    if (!items.length) {
        // if no suggestion matches the user input value, show the user input in the suggestions list
        const userInput = inputBox.value;
        listData = `<li>${userInput}</li>`;
    } else {
        //returns list array as a joined string
        listData = items.join('');
    }
    //display suggestions to UI
    suggBox.innerHTML = listData;
}

function addItem(name) {
    for (let i = 0; i < shoppingCart.length; i++) {
        // check if there is already an item in the shoppingCart with the same name
        if (shoppingCart[i].name === name) {
            //if there is, add one to the quantity of that item
            shoppingCart[i].qty += 1;
            return;
        }
    }

    // if the item is not already present in shoppingCart, push an item object into the array
    const item = { name: name, qty: 1 }
    shoppingCart.push(item);

}

// page selection buttons (which is not ideal, but works for the sake of the prototype)
btnToGroceryList.addEventListener("click", () => {
    document.querySelector(".page-one").classList.remove("hidden");
    document.querySelector(".page-two").classList.add("hidden");
})

btnToStoreSelection.addEventListener("click", () => {
    //checks if something has been added to the cart already
    if (shoppingCart.length > 0) {
        document.querySelector(".page-one").classList.add("hidden");
        document.querySelector(".page-two").classList.remove("hidden");
    }
})

btnBackToStoreSelection.addEventListener("click", () => {
    //checks if something has been added to the cart already
    document.querySelector(".page-three").classList.add("hidden");
    document.querySelector(".page-two").classList.remove("hidden");
})


btnToStorePathway.addEventListener("click", () => {
    if (selectedStore !== "") {
        document.querySelector(".page-two").classList.add("hidden");
        document.querySelector(".page-three").classList.remove("hidden");
        selectedStoreText.innerHTML = selectedStore;
    }
})


// changing the style of selected store
for (let i = 0; i < storeItems.length; i++) {
    storeItems[i].onclick = function () {
        if (storeItems[i].style.backgroundColor === "white") {
            storeItems[i].style.backgroundColor = "black";
            storeItems[i].style.color = "white";
        }
        else {
            storeItems[i].style.backgroundColor = "white";
            storeItems[i].style.color = "black";
        }
        selectedStore = storeName[i].innerHTML + " " + storeAdress[i].innerHTML;
    };
}

// change the innerHTML of output to show the currently selected value of the slider
slider.oninput = function () {
    output.innerHTML = "Distance: " + this.value + " kilometers max";
}

// toggle the filter popup on and ioff
btnFilter.addEventListener("click", () => {
    filterElements.classList.toggle("hidden");
})

// insert pseudo location on click
btnLocation.addEventListener("click", () => {
    locationInput.value = "Åbogade 15, 8200 Aarhus N";
})

// function that executes upon selecting one of the li elements
function select(element) {
    let selectedItem = element.textContent;

    //displaying the selected item as the new text in the input box
    inputBox.value = selectedItem;

    // executes when clicking the magnifying glass
    btnSearch.onclick = () => {

        //adds an item to the shopping cart
        addItem(selectedItem);

        //empty string that gets populated with the contents of the shoppingCart array
        let itemString = "";
        for (let i = 0; i < shoppingCart.length; i++) {
            // an object with a name and quantity is inserted into the shoppingCart array at index i
            const { name, qty } = shoppingCart[i];

            // this information is added to the itemString string, displaying the information to the user
            itemString += `<li>
            <span class="quantity">${qty}</span><img src="picture.jpg" height="40px">${name}<span class="close">x</span>
            </li>`;
        }

        //update the shopping list with the contents of itemString
        list.innerHTML = itemString;

        // blue "X" close buttons
        const close = document.querySelectorAll(".close");
        for (let i = 0; i < close.length; i++) {
            //adding event listeners to every close button
            close[i].addEventListener("click", () => {
                // remove closed element from the UI
                close[i].parentElement.style.display = "none";
                //remove closed element from shoppingCart array
                shoppingCart.splice(i, 1);
            })
        }

        // empty the value of the search bar, so the user can search for a new item
        inputBox.value = "";
    }
    // remove the autocomplete box
    inputWrapper.classList.remove("active");
}

// if the user presses any key within the input field and releases it, execute the below function
inputBox.onkeyup = (e) => {
    let userData = e.target.value; //refers to the data input by the user 
    let emptyArray = [];
    if (userData) {
        //if the user has input anything, populate emptyArray with all elements that pass the test implemeted by the below function
        emptyArray = suggestions.filter((data) => {

            return data.toLocaleLowerCase().startsWith(userData.toLocaleLowerCase());

        });
        //the map() method creates a new array where every item is converted to a <li> element
        emptyArray = emptyArray.map((data) => {
            // displaying data as a li tag
            return data = `<li>${data}</li>`;
        });

        //show autocomplete box
        inputWrapper.classList.add("active");

        //function that displays suggestions based on user input
        showSuggestions(emptyArray);

        // selecting all li items in the autocomplete box
        let allList = suggBox.querySelectorAll("li");
        for (let i = 0; i < allList.length; i++) {
            //adding an onclick attribute to all li tags individually
            allList[i].setAttribute("onclick", "select(this)");
        }
    } else {
        inputWrapper.classList.remove("active"); //hide autocomplete box
    }
}
