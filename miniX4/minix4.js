let textAboutYou;

let nameInput;
let nameInputQuestion;

let ageDropdown;
let ageInputQuestion;

let gRadio;
let gInputQuestion;

let button;

let ctracker;
let capture;
var mapImg;

//starting number of data points
var dataPoints = 195;

function setup() {
setInterval(countDataPoints, 1000);

createCanvas(windowWidth, windowHeight);

capture = createCapture(VIDEO);
capture.size(640, 480);
capture.hide();

ctracker = new clm.tracker();
ctracker.init(pModel);
ctracker.start(capture.elt);

//name input
nameInput = createInput();
nameInput.position(100, 200);


//text inviting user to interact
textAboutYou = createElement('h3', 'Fill in your details below, please.');
textAboutYou.position(nameInput.x, nameInput.y - 110);
textAboutYou.style('color', '#fff');
textAboutYou.style('font-family', 'arial');

//Name text
nameInputQuestion = createElement('h4', 'Name');
nameInputQuestion.position(nameInput.x, nameInput.y - 45);
nameInputQuestion.style('color', '#fff');
nameInputQuestion.style('font-family', 'arial');

//Age input
ageDropdown = createSelect();
ageDropdown.position(nameInput.x, nameInput.y + 55);
//For-loop looping through all years between 1921 and 2021
for (i = 1921; i < 2022; i++)
{
ageDropdown.option(i);
ageDropdown.value(i);
}

//Year of birth text
ageInputQuestion = createElement('h4', 'Year of birth');
ageInputQuestion.position(nameInput.x, nameInput.y + 10);
ageInputQuestion.style('color', '#fff');
ageInputQuestion.style('font-family', 'arial');

//Gender select
gRadio = createRadio();
gRadio.position(nameInput.x, nameInput.y + 95);
gRadio.style('color', '#fff');
gRadio.option("Male");
gRadio.option("Female");
gRadio.option("Non-binary");
gRadio.style('font-family', 'arial');

//Gender text
gInputQuestion = createElement('h4', 'Gender');
gInputQuestion.position(nameInput.x, nameInput.y + 55);
gInputQuestion.style('color', '#fff');
gInputQuestion.style('font-family', 'arial');

//Submit button
button = createButton('Submit my data');
button.position(nameInput.x, nameInput.y + 120);
button.mousePressed(displayInput);
button.style('font-family', 'arial');
}

//function that increments data points with a random value between two values
function countDataPoints(){
  dataPoints = dataPoints + floor(random(100, 6000));
  console.log(dataPoints);
}

function draw() {

  background(0);
  //Threshold video capture
  image(capture, 400, 180, 320, 240);
  filter(THRESHOLD);

  //incrementing dataPoints value every second
  fill(255);
  textFont('Arial');
  text("Mining your data... current amount of data points fetched from you: " + dataPoints, nameInput.x, nameInput.y - 100);

//Face tracking dots
  let positions = ctracker.getCurrentPosition();

  if (positions.length) {

    for (let i = 0; i < positions.length; i++) {
       noStroke();

       fill(0, 41, 250);
       ellipse(positions[i][0], positions[i][1], 5, 5);

       fill(141, 7, 246);
       ellipse(positions[i][0] + 400, positions[i][1], 5, 5);

       fill(255, 255, 5);
       ellipse(positions[i][0] + 600, positions[i][1] + 200, 5, 5);

       fill(255, 5, 255);
       ellipse(positions[i][0] + 200, positions[i][1] + 200, 5, 5);
    }
  }
}


function displayInput() {
//gets your name from the input field
  const name = nameInput.value();

//calculates your age based on your birth year selection
  const age = 2021 - ageDropdown.value();

//gets your gender from the radio selection
  const gender = gRadio.value();

//variable that changes wording depending on age + gender
  let lifeStage;

//if person is a child between ages 0-17
  if(age <= 17)
  {
    if(gender == "Female")
    {
    lifeStage = ("a girl around the age of " + age + ".");
    }
    else if(gender == "Male")
    {
    lifeStage = ("a boy around the age of " + age + ".");
    }
    else
    {
    lifeStage = ("a child around the age of " + age + ".");
    }
  }

//if person is a young adult between ages 18-25
  else if(age <= 25)
  {
    if(gender == "Female")
    {
    lifeStage = ("a young woman around the age of " + age + ".");
    }
    else if(gender == "Male")
    {
    lifeStage = ("a young man around the age of " + age + ".");
    }
    else
    {
    lifeStage = ("a young non-binary person around the age of " + age + ".");
    }
  }

//if person is an adult aged 26-45
  else if(age <= 45)
  {
    if(gender == "Female")
    {
    lifeStage = ("a woman around the age of " + age + ".");
    }
    else if(gender == "Male")
    {
    lifeStage = ("a man around the age of " + age + ".");
    }
    else
    {
    lifeStage = ("a non-binary person around the age of " + age + ".");
    }
  }

//if person is middle-aged between 46-65
  else if(age <= 65)
  {
    if(gender == "Female")
    {
    lifeStage = ("a middle-aged woman around the age of " + age + ".");
    }
    else if(gender == "Male")
    {
    lifeStage = ("a middle-aged man around the age of " + age + ".");
    }
    else
    {
    lifeStage = ("a middle-aged non-binary person around the age of " + age + ".");
    }
  }

//all ages 66-100 years old
  else
  {
    if(gender == "Female")
    {
    lifeStage = ("an elderly woman around the age of " + age + ".");
    }
    else if(gender == "Male")
    {
    lifeStage = ("an elderly man around the age of " + age + ".");
    }
    else
    {
    lifeStage = ("an elderly non-binary person around the age of " + age + ".");
    }
  }

//Text to be printed
  textAboutYou.html('Thanks for your contribution, ' + name + '. We can now start brainwashing<br>you with personalized ads based on the fact that we know you are<br>' + lifeStage);
  nameInput.value(name);
}
