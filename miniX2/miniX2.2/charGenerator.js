let myFont;

//declaration of various arrays that are generated
var skinColor = ['#583b2b', '#503335', '#8d5524', '#c68642', '#e0ac69', '#f1c27d', '#ffdbac', '#f2cba5', '#f5d8ba'];
var hairColor = ['#111111', '#3a251d', '#604733', '#ac5333', '#ac5333', '#d1b350', '#fff2c8', '#e3e4e4', '#525252', '#78644b', '#FFFFFF', '#e96bb3', '#4dace3'];
var eyeColor = ['#3d3713', '#153649', '#154915', '#492a15', '#5f3b23', '#315a13', '#2e4757', '#393939', '#493115', '#5c7584'];
var shirtColor = ['#6ca39d', '#9cc3be', '#272727', '#eadfb1', '#dacc8b', '#2b3956', '#8a7184', '#e09d55', '#523738'];

var charName =
['Jonatan',
'Jeevith',
'Peter',
'Frederik',
'Tobias',
'Emil',
'Oscar',
'Christian',
'Rasmus',
'Lucas',
'Mathias',
'Jonas',
'Mads',
'Casper',
'Morten',
'Noah',
'August',
'Victor',
'Vilhelm',
'Sebastian',
'Theodor',
'Benjamin',
'Alexander',
'Alfred',
'William',
'Eric'];

//thanks to The Sims for inspiration (:
var charTrait =
['n active',
' cheerful',
' creative',
' romantic',
' neat',
' family-oriented',
' jealous',
' mean',
'n outgoing',
' self-absorbed',
'n adventurous',
'n ambitious',
'n awkward',
' sophisticated',
' thoughtful',
' fun-loving',
' chatty',
' easy-going',
' friendly',
' funny',
' handsome',
' generous',
' hard-working',
' lazy'];

var charNoun =
[' bookworm.',
' cat lover.',
' artist.',
' music lover.',
' perfectionist.',
' snob.',
' vegetarian.',
' loner.',
' kleptomaniac.',
' intellectual.',
' dog lover.',
' geek.',
' foodie.',
' gamer.',
' bird-enthusiast.',
' idealist.',
' night-owl',
' workaholic.',
' man-baby.'];

var charLikes =
['eating spaghetti.',
'playing videogames.',
'hanging out with friends.',
'watching horror movies.',
'playing the guitar.',
'walking his dog.',
'playing with his cat.',
'knitting.',
'playing soccer.',
'cooking.',
'painting.',
'taking long hot baths.',
'spending time with his girlfriend.',
'spending time with his boyfriend.',
'bubble tea.',
'photography.'];

var charDislikes =
['the lockdown.',
'cleaning his house.',
'doing math homework.',
'when his neighbor sings loudly.',
'washing the dishes.',
'having dinner with his in-laws.',
'going to the gym.',
'public transport.',
'awkward silences.',
'shallow people.',
'milk.',
'bad drivers.'];

var charFact =
['he likes the smell of his own farts.',
'he likes to dress up as Mickey\nMouse.',
'he actually believes in the tooth\nfairy.',
'he is in love with Jennifer Lopez.',
'he always wanted to be a pilot.',
'his cat is named McMuffins.',
'he once killed a person.',
'he likes piña coladas.',
'he has 7 siblings.',
'he has a swimming pool at home.',
'his grandfather is the pope.',
'he likes to collect pottery.',
'he is really good at beer pong.',
'he is terrified of doves.',
'he has a tattoo on his ankle.',
'he loves dubstep.',
'he owns a pet iguana named\n George.'];

function preload()
{
  myFont = loadFont('pingwing.ttf');
}

function setup()
{
createCanvas(windowWidth,windowHeight);
background(200);

//skin- hair- and eyecolor is randomized upon resetting the page
skinColor = random(skinColor);
hairColor = random(hairColor);
eyeColor = random(eyeColor);
shirtColor = random(shirtColor);

//t-shirt
noStroke();
fill(shirtColor)
rect(90, 530, 220, 100, 90, 90, 90, 90);

//neck

  fill(skinColor)
  rect(175, 500, 50, 80, 90, 90, 90, 90);

//shadow overlay (basically a purple overlay with low alpha creating a shaded variant of the skincolor)
  fill(94, 18, 52, 30);
  rect(175, 500, 50, 80, 90, 90, 90, 90);

//hair
  fill(hairColor);
  rect(75, 85, 250, 150, 90, 90, 30, 30);

//ears
  fill(skinColor);
  ellipse(110, 265, 80, 80);
  ellipse(290, 265, 80, 80);

//ears shadow overlay
  fill(94, 18, 52, 30);
  ellipse(110, 265, 80, 80);
  ellipse(290, 265, 80, 80);

//another overlay
  fill(94, 18, 52, 30);
  ellipse(102, 265, 30, 40);
  ellipse(298, 265, 30, 40);

//head
  fill(skinColor);
  noStroke();
  strokeWeight(4);
  rectMode(CENTER);
  rect(200, 320, 200, 400, 70, 70, 90, 90);

//hair components that are drawn on top of the head
  fill(hairColor);
  ellipse(150, 120, 100, 50);
  ellipse(250, 120, 100, 50);

//eyebrows
  noStroke();
  fill(hairColor);
  rect(140, 175, 70, 25, 90, 90, 90, 90);
  rect(260, 175, 70, 25, 90, 90, 90, 90);

//nose (same kind of overlay as ears)
  fill(94, 18, 52, 30);
  rect(200, 280, 30, 70, 90, 90, 90, 90);

//eyes
  fill(eyeColor);
  rectMode(CENTER);
  rect(260, 220, 40, 40, 90, 90, 5, 5);
  rect(140, 220, 40, 40, 90, 90, 5, 5);

//pupils
  fill(0, 0, 0, 80);
  ellipse(144, 216, 30, 30);
  ellipse(264, 216, 30, 30);

//white reflections in eyes
  fill(230);
  ellipse(150, 210, 10, 8);
  ellipse(270, 210, 10, 8);

//rosy cheeks
  fill(194, 18, 52, 30);
  ellipse(130, 295, 50, 50);

  ellipse(270, 295, 50, 50);

//mouth
  fill(94, 18, 52, 95);
  arc(200, 370, 130, 100, 0, PI);

//text box
  rectMode(CORNER);
  fill(230);
  stroke(shirtColor);
  strokeWeight(5);
  rect(370, 130, 540, 365, 30, 30, 30, 30);

//text components that display the various generated traits
  fill('#3d3713');
  noStroke();
  textSize(25);
  text('Name: ' + (random(charName)), 410, 240);

  textSize(25);
  text('Age: ' + floor(random(15, 56)), 410, 280);

  textSize(25);
  text('He is a' + (random(charTrait)) + (random(charNoun)), 410, 320);

  textSize(25);
  text('He likes ' + (random(charLikes)), 410, 360);

  textSize(25);
  text('He dislikes ' + (random(charDislikes)), 410, 400);

  textSize(25);
  text('Fun fact: ' + (random(charFact)), 410, 440);

  textFont(myFont);
  textSize(50);
  fill(shirtColor);
  stroke(50);
  strokeWeight(3);
  text('Character generator', 380, 125);

  button = createButton('Randomize character');
  button.position(540, 160);
  button.mouseClicked(reloadWindow);
  button.style('font-size', '20px');
  button.style('background-color', shirtColor);
  button.style('color', '#ffffff');

}

//function that reloads window when button is pressed
function reloadWindow()
{
  location.reload();
}
