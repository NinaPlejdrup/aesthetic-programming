/*declaration of arrays and a variable representing 5 colors,
the sandColor being "fill in" sand colors, the greenColor being a bright green
colorblind people are able to see, and the mossColor being a more desaturated
green that colorblind people cannot see*/

var sandColor = ['#ca7c4b', '#e9a873'];
var greenColor = '#40aa6f';
var mossColor = ['#8e9850', '#c5bf73'];

//buttons
let button;
let button2;

/* functions that are executed upon clicking the buttons reassign color values
and rerun the setup() function in order to generate new circles */
function colorblindMode()
{

  sandColor = ['#b48b4f', '#d8b379'];
  greenColor = '#659c6c';
  mossColor = ['#9a9451', '#ccbe77'];
  setup();
}

function normalMode()
{

  sandColor = ['#ca7c4b', '#e9a873'];
  greenColor = '#40aa6f';
  mossColor = ['#8e9850', '#c5bf73'];
  setup();
}

//setup function
function setup()
{
  createCanvas(windowWidth, windowHeight);

//background fill color
  background(200);

// creation of buttons
  button = createButton('Colorblind mode');
  button.position(310, 40);
  button.mousePressed(colorblindMode);

  button2 = createButton('Normal mode');
  button2.position(200, 40);
  button2.mousePressed(normalMode);

//off-white background circle of emoji
  noStroke();
  ellipseMode(CENTER);
  fill(230);
  ellipse(300, 300, 410, 410);

//text
  fill(0);
  textSize(25);
  text('Do you see a smiley face?', 160,540);

//green eyes visible to color blind people
  fill(greenColor);
  let randomSize = floor(random(25, 41));
  ellipse(250, 268, randomSize, randomSize);

  ellipse(365, 268, randomSize, randomSize);

//sand colored filler circles in center of emoji
  fill(random(sandColor));
  ellipse(333, 245, randomSize, randomSize);

  ellipse(233, 302, randomSize, randomSize);

  ellipse(280, 235, randomSize, randomSize);

  ellipse(366, 307, randomSize, randomSize);

//outer circle with sand colored circles
  translate(300, 300);
  for (let i = 0; i < 30; i ++)
  {
  let randomSize = floor(random(25, 41));
  fill(random(sandColor));
  ellipse(125, 125, randomSize, randomSize);
  rotate(PI/15);
  }

//2nd circle with sand colored circles
  for (let i = 0; i < 20; i ++)
  {
  let randomSize = floor(random(25, 41));
  fill(random(sandColor))
  ellipse(100, 100, randomSize, randomSize);
  rotate(PI/10);
  }

//mossy mouth invisible to colorblind people
angleMode(RADIANS);
      rotate(5.7);
    for (let i = 0; i < 8; i ++) {
        let randomSize = floor(random(25, 41));
      fill(random(mossColor))
      ellipse(75, 75, randomSize, randomSize);
      rotate(PI/8);
    }

//half circle with sand colored circles fulfilling circle w/ mouth
  for (let i = 0; i < 8; i ++)
  {
  let randomSize = floor(random(25, 41));
  fill(random(sandColor));
  ellipse(75, 75, randomSize, randomSize);
  rotate(PI/8);
   }

   //small half circle with sand colored circles
    rotate(PI/8);
     for (let i = 0; i < 4; i ++)
     {
     let randomSize = floor(random(25, 41));
     fill(random(sandColor));
     ellipse(50, 50, randomSize, randomSize);
     rotate(PI/5);
      }

  //4 cirles in middle of emoji with sand colored circles
    for (let i = 0; i < 4; i ++)
    {
    let randomSize = floor(random(25, 41));
    fill(random(sandColor))
    ellipse(20, 20, randomSize, randomSize);
    rotate(PI/2);
     }
}

function draw()
{

 }
