# MiniX2.1 - Colorblind Emoji
[Link to RunMe - Colorblind Emoji](https://ninaplejdrup.gitlab.io/aesthetic-programming/miniX2/miniX2.1/)

![Screenshot](miniX2/miniX2.1/screenshot.png)

###### Describe your program and what you have used and learnt
The first emoji is a smiley face made of various dots with randomly generated sizes and colors chosen from a selection of arrays. In order to make the ellipses follow the pattern of a circle with even spacing and the same distance from a pivot point, I looked at a "for loop" provided in the required reading for this week ("Simple Shapes", p5.js) that demonstrated how to use code to draw a flower. The for loop simply loops a piece of code a certain amount of times, in this case drawing ellipses and afterwards rotating itself a certain amount of degrees using the rotate(x/PI); function. In that way, a new circle will be drawn at the new rotated position. After that, it will rotate again and draw a new one etc. Playing around with this for loop on one screen and seeing the drawing update on another helped me gain an intuitive understanding of it that allowed me to make the rest of the emoji primarily using this technique.

The majority of the smiley is made up of sand-colored dots in colors that are randomly selected from an array with two elements. The smiley has two bright green dots for eyes. The mouth is made of moss-colored dots that are also randomly selected from an array with two elements. These colors were carefully selected from a so-called "Ishihara color test plate", that allows people to test whether they are red-green color blind. I thought it was interesting to have the sizes and colors randomly generated, and also I felt it would suit the look of the original test plate more. However, it had to be carefully planned in order not to reveal the pattern by the placement of the circles instead of the colors, allowing colorblind people to guess what I was trying to hide from them. I learned a lot playing around with these various arrays. The interesting thing about this emoji is that people with red-green colorblindness are in fact unable to see the moss-colored mouth of the emoji, and thus they will miss out on the expression. If the user presses the button "Colorblind Mode", colorblind vision is mimicked in order to illustrate this. A click on "Normal Mode" will reverse it.

###### How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on?
Looking at the various emojis, I realized that none of them represent colorblindness, and that colorblind people might run into problems from time to time when interacting with UI if the color scheme is not contrasted enough. In some cases, UI even prevents colorblind people from doing certain things - they are not allowed to operate trains or airplanes as far as I know for example, and that is because of the typical red/green color coding of UI.

# MiniX2.2 - Character Generator
[Link to RunMe - Character Generator](https://ninaplejdrup.gitlab.io/aesthetic-programming/miniX2/miniX2.2/)

![Screenshot](miniX2/miniX2.2/screenshot.png)

###### Describe your program and what you have used and learnt
In the miniX2.1, I felt like I kind of didn't do as the exercise told me as I mainly used circles to make the emoji. I therefore decided to create a more complex character using various shapes and parameters. The result is a character generator, where I use variables and arrays to generate a character with random looks and traits. I learned a lot about how to use variables and call them on multiple occacions, and designing the colors so that it is possible to create a coherent character. I also played around with alpha overlays, mimicking shadows on the neck, inside ears etc.

![Screenshot](miniX2/miniX2.2/promoPic.png)

###### How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on?
This program is meant to celebrate the diversity of looks, personality and quirks. I tried to supply the arrays with as many color combinations as possible, and at the same time not make some colors look washed out or underprioritized. This was actually really difficult to do with the darker skin-tones. A further development of the program could be to amplify the contrast of the facial elements if a dark skin tone is selected. I also tried to make a wide range of interests, traits and character quirks to emphazise the fact that everyone is different. Some of the traits or descriptions are nice, while others are less ideal. In the future I would like to look into whether it is possible to state the likelyhood of each element showing up, intentionally making some traits more rare to encounter. In the "Likes" category, "spending time with his boyfriend" is an element in the array, so sometimes the generated character will be in a homosexual relationship :)
