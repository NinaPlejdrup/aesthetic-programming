//Google Custom Search API related variables
let url = "https://www.googleapis.com/customsearch/v1?";
let apikey = "AIzaSyDwNN2Mz5uK2ZDk_cBVMJXk903DrzpzORM";
let engineID = "7d96eb2ba55c7e9c7";
let query = "danish+culture";
let searchType = "image";
let imgSize ="medium";
let request;
let getImages;

//variables related to the flag graphic and position
let flag;
let flagPosX;
let flagPosY;

//DOM-element variable declarations
let termsButton;
let checkbox;
let submitButton;
let boxIsChecked;

//statistics json
let json;

function preload(){
//loads in the flag image before loading the rest of the program
flag = loadImage('Flag.png');

//the statistics from Danmarks Statistik
json = loadJSON('numbers.json');
}

function setup() {
//setup of canvas
createCanvas(windowWidth, 1200);
background(0);

//all images are fetched from the Google API once upon setup
fetchImages();

//initializing of the flag position values in setup because other elements are relative to this
//placed in center of the window on the x-axis
flagPosX = windowWidth/2;
flagPosY = 620;

//boxIsChecked is per default set to false before the checkbox is checked by user
boxIsChecked = false;

//title properties
textAlign(CENTER);
textStyle(BOLD);
textSize(80);
fill(199, 4, 44);
text('WELCOME TO DENMARK', windowWidth/2, 90);

//"welcome text" properties
textStyle(NORMAL);
textSize(20);
fill(255);
text('Welcome to Denmark! A country with a stable government, diverse natural areas and low crime rates. As a Danish citizen, you can look forward to enjoying world-class quality of life and become one of the happiest people in the world. With its free education and healthcare, Denmark ranks amongst the countries with the highest quality of living.',
 windowWidth/2 - 400, 130, 900, 200);

//// DOM-ELEMENTS
//terms and conditions button
termsButton = createButton("Read terms and conditions");
termsButton.position(flagPosX - 100, flagPosY + 320); //button placement is relative to the flag position with an offset
termsButton.style('font-size', '15px');
termsButton.style('font-family', 'Arial');
termsButton.mousePressed(termsAndConditions);

//checkbox
checkbox = createCheckbox('I have accepted the terms and conditions', false);
checkbox.style('color', 'white');
checkbox.position(flagPosX - 100, flagPosY + 350); //box placement is relative to the flag position with an offset
checkbox.changed(checkedBox); //checks if the box has been clicked, but not whether it is checked or unchecked
checkbox.style('font-size', '15px');
checkbox.style('font-family', 'Arial');

//submit button
submitButton = createButton("Submit");
submitButton.position(flagPosX - 100, flagPosY + 380); //button placement is relative to the flag position with an offset
submitButton.mousePressed(clickSubmit);
submitButton.style('font-size', '15px');
submitButton.style('font-family', 'Arial');
}

function fetchImages() {
/* function that composes an URL for the search query consisting of the starting URL snippet, the API key,
the engine ID, the requested image size, query and search type*/
request = url + "key=" + apikey + "&cx=" + engineID + "&imgSize=" + imgSize +
"&q=" + query + "&searchType=" + searchType;

//prints this URL to the console for easy debugging
console.log(request);

/*
This for-loop adds the snippet &start= + a value (i*10) to the URL, basically generating the numbers
0, 10, 20, 30, 40, 50, 60, 70, 80 and 90 as values. This tells the API to start with that search result number
when requesting images, because the results are grouped in pages of 10 so you have to tell the API to move on
to the next 10 items in order to access 100 images.
*/
for (let i = 0; i < 10; i ++) {
//gotData is a callback function that runs when the .json is loaded successfully
loadJSON(request+"&start="+(i*10), gotData);
}
}

function gotData(data) {
//for every page accessed, access all 10 items in the array from 0 to 9
  for (let i = 0; i < 10; i ++)
{
getImages = data.items[i].image.thumbnailLink;

//if the data has been retrieved, display the images
if (getImages){
loadImage(getImages, img=> { //img is a callback function that runs when the image is loaded
scale(1);

//images are placed within the positions of the flag that are stated within setup()
//the hard coded numeric values are placed because there was an offset
image(img, random(flagPosX - 350, flagPosX+(flag.width/2) - 260), random(flagPosY - 50, flagPosY+(flag.height/2)) - 200);
    });
  }
}

//lower opacity
tint(255, 128);
imageMode(CENTER);
//placement of the flag
image(flag, flagPosX, flagPosY, flag.width/1.5, flag.height/1.5);
}

function draw() {
//empty, might delete later

}

function termsAndConditions(){
  alert('- Don\'t just show up at your friends’ houses without having something planned beforehand \r\n- Drink alcohol rigorously \r\n -You have to eat pork at least once a week \r\n -You have to be a Christian, but not too much \r\n -Dinner should be eaten between 5 and 7 pm \r\n -You shouldn\'t ask "How are you" unless you want an actual answer \r\n -You have to adapt to the law of Jante \r\n -You have to love the Danish flag. Everywhere. \r\n -You have to be cool with hitting a cat in a barrel and sacrificing a witch once a year for Fastelavn and Skt. Hans respectively \r\n -You have to be punctual You have to understand and use sarcasm');
}

/* function that executes when the checkbox is checked. It basically sets the boolean "boxIsChecked" to true or false,
because the user needs to check the checkbox first before clicking "Submit" */
function checkedBox(){
/* this swtiches between true and false statements when the box is checked and unchecked,
  ensuring that the user cannot proceed if they check and uncheck */
if(boxIsChecked == false){
    boxIsChecked = true;
  }
else{
    boxIsChecked = false;
  }
}

//function that is executed when the submit button is clicked
function clickSubmit(){
  //show statistics only if checkbox is checked
  if(boxIsChecked == true){
//black box overlaying the flag
fill(0);
noStroke();
rect(windowWidth/2 - 410, 300, 850, 850);

//Data text
fill(255);
textAlign(LEFT);
textSize(20);
text("Descendants:",windowWidth/2 - 370, 550);
text("Immigrants:",windowWidth/2 - 370, 600);
text("Danish origin:",windowWidth/2 - 370, 650);
text("Total population:",windowWidth/2 - 370, 700);

//Source text
textAlign(CENTER);
textSize(10);
text("Source: https://statistikbanken.dk/", windowWidth/2, 750);

//Bars
stroke(255);
strokeCap(SQUARE);
strokeWeight(8); // changes the width of the line

line(windowWidth/2 - 200, 540, windowWidth/2 - 200 + json.descendant , 540);
line(windowWidth/2 - 200, 590, windowWidth/2 - 200 + json.immigrants , 590);
line(windowWidth/2 - 200, 640, windowWidth/2 - 200 + json.danishOrigin , 640);
line(windowWidth/2 - 200, 690, windowWidth/2 - 200 + json.total , 690);
  }

//else, if the checkbox is not checked, alert the user
  else if(boxIsChecked == false)
  {
    alert('You must accept the Terms and Conditions in order to submit!');
  }
}
