# MiniX9 by Marie Louise Hansen and Nina Plejdrup Hansen
[Link to RunMe](https://ninaplejdrup.gitlab.io/aesthetic-programming/miniX9/)

![Screenshot](miniX9/screenshot.png)

###### What is the program about? Which API have you used and why?
The program is a search engine that searches amongst different New York Times articles. We used an API from New York Times in our program. Therefore, it is only articles from this publisher which are available through our program. This is stated in the title of the program. 

###### Can you describe and reflect on your process in this miniX in terms of acquiring, processing, using, and representing data?
In digital culture, the API’s ensure a smooth, easy and intuitive use of digital products, just as they ensure an easier creation of digital products. The process of actually acquiring an API was quite difficult. We spent quite a while trying to find an API that was free to use and also contained something meaningful. After a while we decided to use the New York Times API for our project. Using a chrome extension that turns a raw API into something more easily readable, we were better able to understand the API and the data it provided.

Platform providers are able to choose which data goes into the API - just like how the NYT API has been made by someone who has chosen a set of articles to include in their API. This gives the API provider power over the user, since the API provider can directly influence what data is given and shown to the user. For example, Nina and I could have found an API containing articles with a certain political narrative, and then disguised it in our program as an API containing a wide range of articles with no particular political stance. Doing this can manipulate voters and thus manipulate an election. Therefore, API providers hold a lot of power, as they can determine what is shown to the end-user as well as what isn’t. In our program, we merely state that the user can “get the latest news”, but not from which source they originate. Therefore, the user will be presented with news that could potentially stem from anywhere, since the program doesn’t provide proof of the sources and one therefore cannot be source critical.


###### Try to formulate a question in relation to web APIs or querying/parsing processes that you would like to investigate further if you had more time.
Knowing how data can easily be selected and manipulated, we would like to dive into datasets that have been proven to spread misinformation.
