let url = "https://api.nytimes.com/svc/search/v2/articlesearch.json?q=";
let apikey = "foY2Hoo0IfP5kyyUVOPOROlb1cnruxgK";
let input;
let button;
let resetButton;
let request;

function setup() {
	createCanvas(windowWidth, windowHeight);
	background(255, 241, 179);

	textAlign(LEFT);
	textStyle(BOLD);
	textSize(40);
	fill(240, 96, 127);
	text('Get the latest news here', 380, 50);

 	input = createInput();
	input.position(windowWidth/2 - 100, 80);

	button = createButton('Submit');
	button.position(input.x + 30, input.y + 30);
	button.mousePressed(displayInput);
	button.style('font-family', 'arial');

	resetButton = createButton('Reset');
	resetButton.position(input.x + 90, input.y + 30);
	resetButton.mousePressed(reset);
	resetButton.style('font-family', 'arial');

}

function gotData(data){
		background(255, 241, 179);
		let articles = data.response.docs;

		textAlign(LEFT);
		textStyle(BOLD);
		textSize(40);
		fill(240, 96, 127);
		text('Get the latest news here', 380, 50);

		textAlign(CENTER);
		textSize(30);
		fill(165, 169, 250);
		text(articles[0].headline.main, 250, 150, 800, 800);
		textSize(20);
		text(articles[0].lead_paragraph, 250, 250, 800, 800);
}

function displayInput(){
		let searchInput = input.value();
		request = url + "?q=" + searchInput + "&api-key=" + apikey;
		loadJSON(request, gotData);
		input.value(searchInput);
		console.log(request);
}

function reset(){
location.reload();
 }
