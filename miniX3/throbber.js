// declaration of the color changing variable
  let colorHue = 0;
//sound variable is instantiated
  let music;
//random chill text to be generated
  let textArray =
  ['Are you in a hurry?',
  'Enjoy the peace and quiet.',
  'Just sit back and relax.',
  'Take a moment to breathe.',
  'Smile. You are doing good :)',
  'No rush.',
  'When is the last time you did\nnothing?',
  'Enjoy the view out your window.',
  'Do you hear the birds chirping\noutside?',
  'Breathe in.',
  'Breathe out.',
  'You got all the time in the world.',
  'Be patient.'];

  function preload()
  {
  //sound file is preloaded
  music = loadSound('meditation.mp3');
  }

  function setup()
{
  //music is looped upon setting up
  music.loop();
  //canvas and framerate is set
  createCanvas(windowWidth,windowHeight);
  frameRate(60);
  //the text and the fade effect appear with a set interval
  setInterval(backgroundRect, 200);
  setInterval(textStatement, 4000);
}

function draw()
{
  //I used HSB colormode for this code, as the hue can be easily changed in the first parameter
  colorMode(HSB, 360, 255, 255, 255);
  background(0, 0, 360, 1);
  drawElements();
  //console.log to check if colorHue increments and resets as expected
  console.log('Current hue: ' + colorHue);
}

function drawElements()
{
  //num = number of shapes to be distributed around circle
  // cir = the amount of radians to be rotated each frame
  //frameCount%num spits out every number between 0 and 359
  let num = 360;
  let cir = 360/num*(frameCount%num);

//checks if colorHue has exceeded or is equal to 360 (the max hue value when using HSB colormode)
  if (colorHue >= 360)
  {
    //if it is, reset it back to 0
    colorHue = 0;
  }
  else
  {
    //if it is not, increment the value by 1
    colorHue++;
  }

    //draws and rotates the circles each frame creating a throbber
    push();
    translate(width/2, height/2);
    rotate(radians(cir));
    noStroke();
    fill(colorHue, 360, 360);
    ellipse(50, 20, 40);
    pop();


    //properties of text below throbber
    textAlign(CENTER);

    fill(0, 0, 100);
    textSize(30);
    text('Loading...', width/2, height/2 + 130);

    textSize(20);
    fill(0, 0, 150);
    text('Please stand by', width/2, height/2 + 160);
}

function windowResized()
{
  //reassigns windowWidth and windowHeight upon resizing the canvas
  resizeCanvas(windowWidth, windowHeight);
}

function backgroundRect()
{
  //fills in a white rectangle with 60 alpha on top of the generated statements, creating a fade effect
  fill(0, 0, 360, 60);
  noStroke();
  rectMode(CENTER);
  rect(width/2 - 240, height/2 + 25, 325, 80);
}

function textStatement()
{
  //random statements every 4 seconds
  textAlign(RIGHT);
  noStroke();
  fill(0, 0, 100);
  text(random(textArray), width/2 - 100, height/2 + 10);
}
