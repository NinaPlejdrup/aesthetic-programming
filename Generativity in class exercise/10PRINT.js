let x = 0;
let y = 0;
let spacing = 10;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
}

function draw() {
  stroke(255);
  if (random(1) <0.66) {
    stroke(200,100,255)
    line(x, y, x+spacing, y+spacing);
  } else if (random(1) > 0.66 && random(1) < 0.33){
    noStroke();
    fill(255, 200, 100);
    ellipse(x, y+spacing/2, 10, 10);
  }
  else {
    rectMode(CENTER);
    noStroke();
    fill(100, 255, 200);
    rect(x, y+spacing/2, 10);
  }
  x+=20;
  if (x > width) {
    x = 0;
    y += spacing;
  }
}
