//instantiating of variables
let myFont;
let img;

function preload() {
  //loads custom font
  myFont = loadFont('PressStart2P-Regular.ttf');

  //loads image
  img = loadImage('pepe.gif');
}

function setup()
{
  //put setup code here
  createCanvas(960, 540);
}

function draw()
{
  //put drawing code here
  background(0, 0, 0);

//background pattern
strokeWeight(1);
stroke(0,255,0);


line(-150, 540, 350, 0);
line(-100, 540, 400, 0);
line(-50, 540, 450, 0);
line(0, 540, 500, 0);

  //PINK WINDOW
  //window
  stroke(255, 0, 255);
  fill(0);
  rect(80, 80, 720, 405);

  //upper window bar
  noStroke();
  fill(255,0,255);
  rect(80, 80, 720, 20);

  //cross
  strokeWeight(2);
  stroke(0);
  line(785, 95, 795, 85);

  line(785, 85, 795, 95);

  //small box
  stroke(0);
  noFill();
  strokeWeight(2);
  rect(760, 85, 15, 10);

  //minimize bar
  stroke(0);
  strokeWeight(2);
  line(740, 90, 750, 90);

  //title
  fill(0);
  noStroke();
  textFont(myFont);
  textSize(13);
  text('mousePos.exe',85,97);

//mouse pos display
  fill(255,0,255);
  textSize(20);
  text('Mouse pos: x = ' + mouseX + ', y = ' + mouseY, 90,130);


  //blinking command line
  //makes cmd line color change between magenta and black twice per second using modulo (%) operator
  if (frameCount % 60 < 30)
  {
    fill(0);
    noStroke();
    rect(635, 127, 17, 3);
  }
  else
  {
    fill(255,0,255);
    noStroke();
    rect(635, 127, 17, 3);
  }

  stroke(255,0,255);
  strokeWeight(1);
  fill(0);

  rect(110, 230, 150, 150);

  rect(280, 230, 150, 150);

  rect(450, 230, 150, 150);

  rect(620, 230, 150, 150);


  //GREEN WINDOW
  //window
  stroke(0,255,0);
  fill(0);
  rect(300, 300, 512, 288);

  //upper window bar
  noStroke();
  fill(0,255,0);
  rect(300, 300, 512, 20);

  //cross
  strokeWeight(2);
  stroke(0);
  line(795, 315, 805, 305);

  line(795, 305, 805, 315);


  //small box
  stroke(0);
  noFill();
  strokeWeight(2);
  rect(770, 305, 15, 10);

  //minimize bar
  stroke(0);
  strokeWeight(2);
  line(750, 310, 760, 310);

  //title
  fill(0);
  noStroke();
  textFont(myFont);
  textSize(13);
  text('pepe.exe',305,317);

  //pepe.gif
  image(img, 300, 320, 512, 268);
}
