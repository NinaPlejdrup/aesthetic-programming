class Car{
  constructor(x, y, direction, sprite) //instantiates car at certain position, moving in certain direction and w a certain sprite
  {
    this.xPos = x;
    this.yPos = y;
    this.direction = direction;
    this.sprite = sprite;
  }
show()
{
  //shows the car using the sprite
  image(this.sprite, this.xPos, this.yPos);
}
move()
{
  this.xPos += this.direction; //makes the car move in a certain direction on the x axis every frame

//checks if the car has moved outside the screen and moves it to the opposite end of screen on the x axis
  if(this.xPos < -100)
  {
    this.xPos = width;
  }
  else if(this.xPos > width + 100)
  {
    this.xPos = -100;
  }
}
}
