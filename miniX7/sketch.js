//frogger variables
let froggerSprite;
let froggerSprite2;
let currFroggerSprite;
let frogPosX;
let frogPosY;
let step = 50; //amount of pixels frogger moves
let lives = [];
let onLog = false;

//sound variables
let hopSound;
let squashSound;
let music;
let musicPaused;
let goalSound

//various graphics
let platformSprite;
let goal = [];
let goalImg;
let goalFrog = [];
let goalFrogImg;
let goalBetween;

//cars
let car1Sprite;
let car1 = [];
let car2Sprite;
let car2 = [];
let car3Sprite;
let car3 = [];
let car4Sprite;
let car4 = [];

//logs
let logSprite;
let log1 = [];
let log2 = [];
let log3 = [];
let life = [];
let numOfLives;

//UI
let score;
let scoreText;
let font;
let uiHeight = 50;

function preload(){
//preload of graphics
//frogger
  froggerSprite = loadImage('assets/frogger1.png');
  froggerSprite2 = loadImage('assets/frogger2.png');

//moving objects
  car1Sprite = loadImage('assets/car1.png');
  car2Sprite = loadImage('assets/car2.png');
  car3Sprite = loadImage('assets/car3.png');
  car4Sprite = loadImage('assets/car4.png');
  logSprite = loadImage('assets/log.png');

//various graphics
  platformSprite = loadImage('assets/platform.png');
  goalBetween = loadImage('assets/goalBetween.png');
  goalImg = loadImage('assets/goal.png');
  goalFrogImg = loadImage('assets/goalFrog.png');

  //sounds
    hopSound = loadSound('assets/hop.wav');
    squashSound = loadSound('assets/squash.wav');
    music = loadSound('assets/music.mp3');
    goalSound = loadSound('assets/goal.wav');

//font
  font = loadFont('assets/PressStart2P-Regular.ttf');
}


function setup(){
//starting values
  score = 0;
  numOfLives = 5;
  onLog = false;
  musicPaused = false;
  music.loop();

  noCursor();
  createCanvas(700, 550); //needs to be divisible by step for the game to look good
  restartPosition(); //restarts the game upon launching it

//frogger is instantiated
  frogPosX = width/2 - 50; //frogger is instantiated slightly off-center relative to the canvas
  frogPosY = height-43; //frogger is also instantiated 43 pixels from the bottom (idk why, that just looked best)
  currFroggerSprite = froggerSprite; //sets the current frogger sprite to idle frogger

  //setup of objects
  setupElements();
}

function draw(){
  background(0);

  //checks if frogger has collided with something
  checkCollision();
  //checks if edges of canvas have been reached and contain frogger within them
  checkEdges();

//drawing the water as a rectangle below graphics
  fill(0, 0, 71);
  noStroke();
  rect(0, 0, width, 200);

console.log(onLog);

//check if frogger lands in water
/*if(!onLog && frogPosY <= 185 && frogPosY > 15)
  {
    //if so, play hit sound, subtract a life and restart position
    squashSound.play();
    numOfLives--;
    restartPosition();
  } */

  if(goal[0].hasReached === true &&
    goal[1].hasReached === true &&
    goal[2].hasReached === true &&
    goal[3].hasReached === true &&
    goal[4].hasReached === true)
  {
    winGame(); //the game is won if all goals have been reached
  }

//constantly checks if lives reach 0
  if(numOfLives <= 0)
  {
    die();
  }

  //displays various sprites and objects
  showElements();

  //draws UI elements
    drawUI();
}

function setupElements(){
  //appears in setup
  //cycles though loop and creates specified amount of cars w certain properties
  for (let i = 0; i < 5; i++)
  {
    car1[i] = new Car(100 * i + (80*i), 250, -2, car1Sprite);
  }

  for (let i = 0; i < 3; i++)
  {
    car2[i] = new Car(100 * i + (80*i), 300, -5, car2Sprite);
  }

  for (let i = 0; i < 3; i++)
  {
    car3[i] = new Car(100 * i + (80*i), 350, 3, car3Sprite);
  }

  for (let i = 0; i < 4; i++)
  {
    car4[i] = new Car(100 * i + (130*i), 400, 1, car4Sprite);
  }

  for (let i = 0; i < 4; i++)
  {
    log3[i] = new Log(100 * i + (150*i), 50, 2, logSprite);
  }

  for (let i = 0; i < 3; i++)
  {
    log2[i] = new Log(100 * i + (300*i), 100, -1, logSprite);
  }

  for (let i = 0; i < 43; i++)
  {
    log1[i] = new Log(200 * i + (200*i), 150, 3, logSprite);
  }

  //draws 5 "goals"
    for (let i = 0; i < 5; i++)
    {
      goal[i] = new Goal(22 + (goalImg.width + 44)* i, -10, goalImg);
    }

    for(i = 0; i < goal.length; i++)
    {
        //image(goalFrogImg, goal[i].xPos + 22, goal[i].yPos + 15);
        goalFrog[i] = new GoalFrog(goal[i].xPos + 22, goal[i].yPos + 15, goalFrogImg);
        //goalFrog[i].hide();
    }
}

function showElements(){
  //appears in draw
  for (let i = 0; i < width/platformSprite.width; i++)
      {
        image(platformSprite, platformSprite.width * i, height-100, platformSprite.width, platformSprite.height);
        image(platformSprite, platformSprite.width * i, height-350, platformSprite.width, platformSprite.height);
        image(platformSprite, platformSprite.width * i, height-550, platformSprite.width, platformSprite.height);
      }

      for (let i = 0; i < goal.length; i++)
          {
            //these basically fill the gaps between goals
            //I kinda hardcoded some of this but it looked okay in the end
            image(goalBetween, (goalImg.width + 44)* i, -10, goalBetween.width, goalBetween.height);
            image(goalBetween, goalImg.width + 22 + (goalImg.width + 44)* i, -10, goalBetween.width, goalBetween.height);
          }
//executes various methods for the objects within scene
  for (let i = 0; i < car1.length; i++)
  {
    car1[i].show();
    car1[i].move();
  }

  for (let i = 0; i < car2.length; i++)
  {
    car2[i].show();
    car2[i].move();
  }

  for (let i = 0; i < car3.length; i++)
  {
    car3[i].show();
    car3[i].move();
  }

  for (let i = 0; i < car4.length; i++)
  {
    car4[i].show();
    car4[i].move();
  }

  for (let i = 0; i < log1.length; i++)
  {
    log1[i].show();
    log1[i].move();
  }

  for (let i = 0; i < log2.length; i++)
  {
    log2[i].show();
    log2[i].move();
  }

  for (let i = 0; i < log3.length; i++)
  {
    log3[i].show();
    log3[i].move();
  }

  for (let i = 0; i < goal.length; i++)
  {
    goal[i].show();
  }

//draws frogger at frogPosx, frogPosY
  image(currFroggerSprite, frogPosX, frogPosY, froggerSprite.width, froggerSprite.height);

  for(i = 0; i < goal.length; i++)
  {
    if(goal[i].hasReached === true)
    {
      //image(goalFrog, goal[i].xPos + 22, goal[i].yPos + 15);
      goalFrog.push(new GoalFrog (goal[i].xPos + 22, goal[i].yPos + 15, goalFrogImg));
      goalFrog[i].show();
    }
  }
}

function keyPressed(){
  //both arrow keys and WASD can be used to move frogger around
  if(keyCode === UP_ARROW || keyCode === 87){
    frogPosY -= step;
    //if any of these keys are pressed, change into "leaping" frogger
    currFroggerSprite = froggerSprite2;
    //hop sound
    hopSound.play();
  }
  else if(keyCode === DOWN_ARROW || keyCode === 83){
    frogPosY += step;
    currFroggerSprite = froggerSprite2;
    hopSound.play();
  }
  else if(keyCode === LEFT_ARROW || keyCode === 65){
    frogPosX -= step;
    currFroggerSprite = froggerSprite2;
    hopSound.play();

  }
  else if(keyCode === RIGHT_ARROW || keyCode === 68){
    frogPosX += step;
    currFroggerSprite = froggerSprite2;
    hopSound.play();
  }
  //switches music on/off
  else if(keyCode === 77){
    if(musicPaused){
      music.loop();
      musicPaused = false;
    }
    else if(!musicPaused){
      music.pause();
      musicPaused = true;
    }
  }
}

function keyReleased(){
  //makes frogger look "idle" when keyboard is not touched
  currFroggerSprite = froggerSprite;
}

function checkCollision(){
  //checks whether frogger collides with car of type 1
for(let i = 0; i < car1.length; i++)
{
  let d = int(
  dist(frogPosX + froggerSprite.width/2, frogPosY + froggerSprite.height/2, car1[i].xPos + car1Sprite.width/2, car1[i].yPos + car1Sprite.height/2)
              );
              if(d < froggerSprite.width)
              {
                console.log("Hit by car1");
                squashSound.play();
                numOfLives--;
                restartPosition();
              }
}

  //checks whether frogger collides with car of type 2
for(let i = 0; i < car2.length; i++)
{
  let d = int(
  dist(frogPosX + froggerSprite.width/2, frogPosY + froggerSprite.height/2, car2[i].xPos + car2Sprite.width/2, car2[i].yPos + car2Sprite.height/2)
              );
              if(d < froggerSprite.width)
              {
                console.log("Hit by car2");
                squashSound.play();
                numOfLives--;
                restartPosition();
              }
}

for(let i = 0; i < car3.length; i++)
{
  let d = int(
  dist(frogPosX + froggerSprite.width/2, frogPosY + froggerSprite.height/2, car3[i].xPos + car3Sprite.width/2, car3[i].yPos + car3Sprite.height/2)
              );
              if(d < froggerSprite.width)
              {
                console.log("Hit by car3");
                squashSound.play();
                numOfLives--;
                restartPosition();
              }
}

for(let i = 0; i < car4.length; i++)
{
  let d = int(
  dist(frogPosX + froggerSprite.width/2, frogPosY + froggerSprite.height/2, car4[i].xPos + car4Sprite.width/2, car4[i].yPos + car4Sprite.height/2)
              );
              if(d < froggerSprite.width)
              {
                console.log("Hit by car4");
                squashSound.play();
                numOfLives--;
                restartPosition();
              }
}

//checks whether frogger is within the boundaries of log
for(let i = 0; i < log1.length; i++)
{
              if(frogPosX + froggerSprite.width/2 > log1[i].xPos &&
                frogPosX + froggerSprite.width/2 < log1[i].xPos + logSprite.width &&
              frogPosY + froggerSprite.height/2 > log1[i].yPos &&
            frogPosY + froggerSprite.height/2 < log1[i].yPos + logSprite.height)
              {
                onLog = true;
                console.log("Jumped on log1");
                frogPosX = frogPosX + log1[i].direction;
              }
              else{
                onLog = false;
              }
}

//checks whether frogger is within the boundaries of log
for(let i = 0; i < log2.length; i++)
{
              if(frogPosX + froggerSprite.width/2 > log2[i].xPos &&
                frogPosX + froggerSprite.width/2 < log2[i].xPos + logSprite.width &&
              frogPosY + froggerSprite.height/2 > log2[i].yPos &&
            frogPosY + froggerSprite.height/2 < log2[i].yPos + logSprite.height)
              {
                onLog = true;
                console.log("Jumped on log1");
                frogPosX = frogPosX + log2[i].direction;
              }
              else{
                onLog = false;
              }
}

for(let i = 0; i < log3.length; i++)
{
              if(frogPosX + froggerSprite.width/2 > log3[i].xPos &&
                frogPosX + froggerSprite.width/2 < log3[i].xPos + logSprite.width &&
              frogPosY + froggerSprite.height/2 > log3[i].yPos &&
            frogPosY + froggerSprite.height/2 < log3[i].yPos + logSprite.height)
              {
                onLog = true;
                console.log("Jumped on log3");
                frogPosX = frogPosX + log3[i].direction;
              }
              else{
                onLog = false;
              }
}
              if(frogPosX + froggerSprite.width/2 > platformSprite.x &&
                frogPosX + froggerSprite.width/2 < platformSprite.x + platFormSprite.width &&
              frogPosY + froggerSprite.height/2 > platFormSprite.y &&
            frogPosY + froggerSprite.height/2 < platFormSprite.y + platFormSprite.height)
              {
                onLog = true;
                console.log("Jumped on platform");
              }
              else{
              }

//checks whether frogger has reached one of the goals
for(let i = 0; i < goal.length; i++)
{
              if(frogPosX + froggerSprite.width/2 > goal[i].xPos &&
                frogPosX + froggerSprite.width/2 < goal[i].xPos + goalImg.width &&
              frogPosY + froggerSprite.height/2 > goal[i].yPos &&
            frogPosY + froggerSprite.height/2 < goal[i].yPos + goalImg.height)
              {
                if(goal[i].hasReached === false)
{                goal[i].hasReached = true;
                goalSound.play();
                console.log("Jumped on goal");
                score += 150;
                restartPosition();
              }

                else if(goal[i].hasReached === true)
                {
                squashSound.play();
                restartPosition();
                }
              }
}

}

function checkEdges(){
  //contains frogger within canvas boundaries
  if(frogPosX < 0)
  {
    frogPosX = 0;
  }
  else if(frogPosX > width - step)
  {
    frogPosX = width - step;
  }
  else if(frogPosY < 15)
  {
    frogPosY = 15;
  }
  else if(frogPosY > height-uiHeight-43)
  {
    frogPosY = height-uiHeight-43;
  }
}

function restartPosition(){
  //restarts position when goal is reached or you die
  onLog = false;
  frogPosX = width/2 - 100;
  frogPosY = height-15;
}

function winGame(){
  //goal frogs are removed so that you can start over
  resetGoals();
}

function resetGoals(){
  for(i = 0; i < goal.length; i++)
  {
    //sets all .hasReached to false
  goal[i].hasReached = false;

//empties goalFrog array in order to make the frogs disappear if all .hasReached are false
  if(goal[0].hasReached === false &&
    goal[1].hasReached === false &&
    goal[2].hasReached === false &&
    goal[3].hasReached === false &&
    goal[4].hasReached === false)
    {
    //if all goals have been set to false, empty the goalFrog array to make all goal frogs disappear
    goalFrog = [];
    }
  }
  //goalFrog array is setup anew
  for(i = 0; i < goal.length; i++)
  {
      goalFrog[i] = new GoalFrog(goal[i].xPos + 22, goal[i].yPos + 15, goalFrogImg);
  }
}

function drawUI(){
  //draws black UI box at bottom of screen
  fill(0);
  rect(0, height-uiHeight, width, uiHeight);

//displays current number of lives
  for(let i = 0; i < numOfLives; i++)
  {
      life[i] = new Life(10 + (50 * i), height-uiHeight + 10, froggerSprite); //uiWidth/numOfLives spaces the lives nicely dependent on the width of the UI element
      life[i].show();
  }

//score text
  fill(255, 0, 0);
  textFont(font);
  textSize(23);
  textAlign(LEFT);
  text("Points:"+ score, 280, life[0].yPos + 20);
  textSize(10);
  //text switches dependent on whether music is paused or not
  if(musicPaused){
    text("Hit M to start music", 280, life[0].yPos + 35);
  }
  else if(!musicPaused){
    text("Hit M to pause music", 280, life[0].yPos + 35);
  }
}

function die(){
  numOfLives = 5;
  score = 0;
  resetGoals();
  restartPosition();
}
