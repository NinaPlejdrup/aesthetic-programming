# MiniX8 by Sofus Lindberg and Nina Plejdrup
[Link to RunMe](https://ninaplejdrup.gitlab.io/aesthetic-programming/miniX8/)

![Screenshot](miniX8/screenshot.png)

###### Provide a title of your work and a short description (1,000 characters or less).
The work is called ‘Imprisonment’. The work mixes motivational quotes for inmates with quotes provided by young people for living through the isolation during corona. 

Thus the work demonstrates the similar struggles dealing with isolation as either a prisoner or living through corona. Thus, the real punishment of committing crime becomes clear: The isolation, and how isolation from your loved ones is a real struggle for maintaining a great mental health. Imprisonment is not only a bodily restriction of freedom, but also stripping away a fundamental source for staying mentally healthy.

###### Describe how your program works, and what syntax you have used, and learnt?
For our program, we have drawn a lot of inspiration from the examples at “Text and Type” by Allison Parrish. We took some inspiration from the visual look of the example poem in the section that describes the shuffle() function. The text color is selected using a variable that is relative to the index of the array position, making the color of the quotes go from white to grey. The program generates 5 random items from the .json file every time the mouse is pressed, making 5 random quotes appear. We also learned how to implement and write a .json file, which made the organization of quotes much easier.

###### Analysis and reflection
The decreasing visibility (from white to darker fill value) in the motivational statements, it functions as a metaphor that shows, that human beings despite their best efforts to maintain a positive mindset strongly depend on the circumstances they find themselves in: In the long run, positive quote don’t save you from the fact that isolation takes its toll on one’s mental health. Thus, the positive quotes at first shine in the dark, but eventually, they blend in with the ever constant darkness of isolation.

The software can be linked to Cox’ and McLeans’ idea of ‘translation’. The concept expresses the idea of translating human problems (“loose thinking”) into a logical way of expressing the problem through the software ("strict thinking") _(Geoff Cox and Alex McLean, “Vocable Code,” in Speaking Code (Cambridge, MA: MIT Press,2013, p. 37)_. The work expresses how human emotion of isolation through programming gets a metaphorical layer to express how isolation affects the human mind. 
