let json;
let quote;
let fillValue;
let randomQuote;
let xoff = 0;


function preload(){

//connect to .json
  json = loadJSON('jsonfile.json');
}

function setup() {

  createCanvas(windowWidth, windowHeight);
  background(0);
  quote = json[0].quotes;
  show();

}

function draw() {
//graphics();
}


//text elements
function show(){
  //shuffle(quote,true);
    background(0);

    fill(255);
    textSize(50);
    textAlign(CENTER);
    text("Imprisonment", width/2, 150);

    fill(255);
    textSize(12);
    textAlign(CENTER);
    text("Imprisonment is a project that showcases motivational quotes concerning inmates and quotes written by inmates and shuffles them with advice about living through the pandemic written by 2nd semester Digital Design students.\nCan you guess where the quotes come from?", width/2, height-30);

  for (let i = 0; i < 5; i++){
    randomQuote = random(json[0].quotes);

  //increasing fill value
  fillValue = i*60
  //sets upper limit of fill value to 120
  let fillLimit = map(fillValue,0,255,0,200);
  fill(255-fillLimit);
  textSize(20);
  textLeading(20);
  //text(shuffle(quote,true),100,100+i*20, width/2);
  text(randomQuote,100,200+i*40, width-200);
}
}

function mousePressed(){
  show();
}

/*
function graphics(){

  //colors outside
  xoff = xoff + 0.01;
  if(xoff >= 255)
  {
  xoff = 0;
  }
  let value = noise(xoff);
  let n = map(value,0,1,0,255);
  fill(245,n,66);
  rect(100,100,200,400);



//window
  push();
noFill();
strokeWeight(10);
stroke(30);
noFill();
rect(100,100,200,400);
rect(100,100,200,200);
rect(100,100,100,400);
  pop();

} */
