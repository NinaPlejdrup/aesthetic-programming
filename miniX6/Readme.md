# MiniX6 - Character Creator
[Link to RunMe](https://ninaplejdrup.gitlab.io/aesthetic-programming/miniX6/)


![Screenshot](miniX6/screenshot.png)

###### What have you changed and why?
For this miniX, I chose to revisit [my miniX2.2](https://gitlab.com/NinaPlejdrup/aesthetic-programming/-/tree/master/miniX2#minix22-character-generator) that I made as one of the emojis for that week. Originally I merely created a random character generator with the idea of representation in mind. However, I realized that I wanted to make the user change all the traits and looks of the character to create their own.

###### How would you demonstrate aesthetic programming in your work? How does your work demonstrate the perspective of critical-aesthetics?
The chapter "Preface" in the Aesthetic Programming textbook emphasizes how the "aesthetic" in aesthetic programming not only deals with the sensory phenomena that is the idea of "beauty", but extends beyond that and addresses the wider political and societal movements: 

_"Continuing the discussion of aesthetics, it should be clear that we do not refer to ideas of
beauty as it is commonly misunderstood (aka bourgeois aesthetics), but to political
aesthetics: to what presents itself to sense-making experience and bodily perception"_
 (Aesthetic Programming, p. 14)

In my miniX2.2, my idea with the "emoji" was representing a wide range of looks and traits as a celebration of human diversity. I really wanted the user have fun with the fact that we are all different, both in terms of looks and personality, while playing with my program. For this reason, some of the possible character combinations are nice (eg. "creative intellectual"), while others are a bit less ideal (eg. "lazy man-baby") in order to create some quirky and fun characters. The same goes for the likes, dislikes and fun-facts that will be generated for the character.
 
As for the looks of the character, I really wanted all the possible color combinations to stand out and look good when combined with each other. It occured to me that in some way,it would easily come across as a political statement by including or excluding certain skincolors, for example. As stated in the textbook:

 _"cultural production — which would now naturally include programming — must be seen in a social context"_ (Aesthetic Programming, p. 15)

I know that a lot of debate has been made about the skincolors and hairstyles provided in the popular game The Sims, because they have quite a large black fanbase, yet a lot of the black skin colors provided in game have been critisized as being dull and ashy, and there are only a few "generic" afro hairstyles to choose from. To me, it is sort of interesting what kind of power that lies in the code we write. What appears to be just hex-codes in a series of arrays might exclude some people and make them sad.

In this iteration I reworked the color arrays to make it easier for the user to pick their desired colors, and made sure that the colors would all go well together in order for all colors to be equally appealing to choose. The big thing I have changed is making the various traits and looks selectable individually by the user. This way, I hope the user feel like they can create someone that they can identify with, instead of just having a random character thrown at them like in my miniX2.2. My miniX6 still has its limitations though, as things such as hairstyle, facial hair, skin texture, face shape, piercings, gender etc are not customizable. Those are all aspects I would like to implement if I were to one day publish my work to a wider audience :)

###### What does it mean by programming as a practice, or even as a method for design?

I find the interplay between programming and design interesting. Programming comes with a lot of rules and syntax that might seem overwhelming or even limiting in the beginning. However, once you harness some the principles of programming, it begins to really show its potential as a powerful tool to visualize and generate things you couldn't do without it. Programming becomes more than just logic and syntax, it becomes a way of thinking and unfolding your creativity. As described above, it can even demonstrate a political statement in the subtlest of ways.

###### What is the relation between programming and digital culture?

In order to answer this question, I would like to refer to the quote: 

_"Programming becomes a kind of “force-field” with which to understand material conditions and social contradictions, just as the interpretation of art once operated “as a kind of code language for processes taking place within society.” "_ (Aesthetic Programming, p. 15)

In present day, digital products increasingly shape the way we interact with each other in our everyday life. It has become a way of living and perceiving our world. Therefore, one must keep up with the various movements of the digital worlds, including the emerging programming languages. Fluency in programming languages has become the new kind of literacy, because it allows one to understand what goes on within the devices that surround us, instead of being controlled by them.

