let myFont;
let img;

//declaration of various arrays that are generated
var skinColor = ['#583b2b', '#503335', '#8d5524', '#c68642', '#e0ac69', '#f1c27d', '#f2cba5', '#f5d8ba', '#ffdbac'];
var hairColor = ['#111111', '#3a251d', '#604733', '#78644b', '#bb391f', '#ac5333', '#8b613f', '#d1b350', '#e3d093', '#fff2c8', '#FFFFFF', '#e3e4e4', '#525252', '#b2b2b2', '#e96bb3', '#4dace3', '#20487e','#68a11e'];
var eyeColor = ['#6bb14f', '#c7f07a', '#e9d98e', '#d49f35', '#9d7d54', '#5a3b1f', '#191814', '#346f75', '#5cb9c1', '#a9e3e4', '#8a9fd4', '#cdd6f5', '#9397a5'];
var shirtColor = ['#6ca39d', '#9cc3be', '#a3394d', '#d073ac', '#476126', '#4b4b4b', '#eadfb1', '#e09d55', '#413a9c', '#476a96', '#8a7184',  '#523738'];

var charName =
['Jonatan',
'Jeevith',
'Peter',
'Frederik',
'Tobias',
'Emil',
'Oscar',
'Christian',
'Rasmus',
'Lucas',
'Mathias',
'Jonas',
'Mads',
'Casper',
'Morten',
'Noah',
'August',
'Victor',
'Vilhelm',
'Sebastian',
'Theodor',
'Benjamin',
'Alexander',
'Alfred',
'William',
'Eric'];

//thanks to The Sims for inspiration (:
var charTrait =
['n active',
' cheerful',
' creative',
' romantic',
' neat',
' family-oriented',
' jealous',
' mean',
'n outgoing',
' self-absorbed',
'n adventurous',
'n ambitious',
'n awkward',
' sophisticated',
' thoughtful',
' fun-loving',
' chatty',
' easy-going',
' friendly',
' funny',
' handsome',
' generous',
' hard-working',
' lazy'];

var charNoun =
[' bookworm.',
' cat lover.',
' artist.',
' music lover.',
' perfectionist.',
' snob.',
' vegetarian.',
' loner.',
' kleptomaniac.',
' intellectual.',
' dog lover.',
' geek.',
' foodie.',
' gamer.',
' bird-enthusiast.',
' idealist.',
' night-owl',
' workaholic.',
' man-baby.'];

var charLikes =
['eating spaghetti.',
'playing videogames.',
'hanging out with friends.',
'watching horror movies.',
'playing the guitar.',
'walking his dog.',
'playing with his cat.',
'knitting.',
'playing soccer.',
'cooking.',
'painting.',
'taking long hot baths.',
'spending time with his girlfriend.',
'spending time with his boyfriend.',
'bubble tea.',
'photography.'];

var charDislikes =
['the lockdown.',
'cleaning his house.',
'doing math homework.',
'when his neighbor sings loudly.',
'washing the dishes.',
'having dinner with his in-laws.',
'going to the gym.',
'public transport.',
'awkward silences.',
'shallow people.',
'milk.',
'bad drivers.'];

var charFact =
['he likes the smell of his own\nfarts.',
'he likes to dress up as Mickey\nMouse.',
'he actually believes in the tooth\nfairy.',
'he is in love with Jennifer\nLopez.',
'he always wanted to be a pilot.',
'his cat is named McMuffins.',
'he once killed a person.',
'he likes piña coladas.',
'he has 7 siblings.',
'he has a swimming pool at\nhome.',
'his grandfather is the pope.',
'he likes to collect pottery.',
'he is really good at beer pong.',
'he is terrified of doves.',
'he has a tattoo on his ankle.',
'he loves dubstep.',
'he owns a pet iguana named\nGeorge.'];

function preload()
{
  //font and button image are preloaded
  myFont = loadFont('pingwing.ttf');
  img = loadImage('Dice_small.png');
}

function setup()
{
createCanvas(1280, 580);
background(200);

//text box
fill(230);
stroke('#000000');
strokeWeight(2);
rect(370, 130, 540, 365, 30, 30, 30, 30);

textFont('Helvetica');
fill('#000000');
noStroke();
textSize(15);

//sliders for the various colors, notice that the starting value (3rd parameter) is randomized upon setup
//the max value is set to the length of the respective arrays minus one to make adding/removing colors easier
text('Skin color', 720, 225);
skincSlider = createSlider(0, skinColor.length - 1, random(0, skinColor.length -1), 1);
skincSlider.position(720,230);

text('Eye color', 720, 285);
eyecSlider = createSlider(0, eyeColor.length - 1, random(0, eyeColor.length - 1), 1);
eyecSlider.position(720, 290);

text('Hair color', 720, 345);
haircSlider = createSlider(0, hairColor.length - 1, random(0,hairColor.length - 1), 1);
haircSlider.position(720,350);

text('Shirt color', 720, 405);
shirtcSlider = createSlider(0, shirtColor.length - 1, random(0,shirtColor.length - 1), 1);
shirtcSlider.position(720,410);

//the various traits are randomly generated in functions
  newName();
  newAge();
  newPersonality();
  newInterest();
  newDisinterest();
  newFunFact();

  //buttons
  button = createButton('Randomize all traits');
  button.position(450, 170);
  button.mouseClicked(randomizeAll);
  button.style('font-size', '15px');

  button2 = createButton('Randomize all looks');
  button2.position(725, 170);
  button2.mouseClicked(randomizeLooks);
  button2.style('font-size', '15px');

  nameButton = createImg('Dice_small.png');
  nameButton.position(390, 225);
  nameButton.mousePressed(newName);

  ageButton = createImg('Dice_small.png');
  ageButton.position(390, 265);
  ageButton.mousePressed(newAge);

  personalityButton = createImg('Dice_small.png');
  personalityButton.position(390, 305);
  personalityButton.mousePressed(newPersonality);

  interestButton = createImg('Dice_small.png');
  interestButton.position(390, 345);
  interestButton.mousePressed(newInterest);

  disinterestButton = createImg('Dice_small.png');
  disinterestButton.position(390, 385);
  disinterestButton.mousePressed(newDisinterest);

  funFactButton = createImg('Dice_small.png');
  funFactButton.position(390, 425);
  funFactButton.mousePressed(newFunFact);
}



function updateColors(){
  //checks if sliders have been moved for every frame
  //creates 'i' variable that is set equal to the current value of the slider
  i = skincSlider.value();
  //sets the current skin color to the i index of the skinColor array
  currSkinColor = skinColor[i];

  j = eyecSlider.value();
  currEyeColor = eyeColor[j];

  k = shirtcSlider.value();
  currShirtColor = shirtColor[k];

  l = haircSlider.value();
  currHairColor = hairColor[l];

}

function draw(){
  //function that checks for color updates every frame
  updateColors();

  //UI circles showing preview of selected colors
  stroke(0);
  strokeWeight(1);

  //skin
  fill(currSkinColor);
  ellipse(880, 240, 30);

  //eyes
  fill(currEyeColor);
  ellipse(880, 300, 30);
  //white shine in eye
  noStroke();
  fill(0, 0, 0, 80);
  ellipse(880, 300, 20);
  //ellipse in eye
  fill(230);
  ellipse(885, 295, 5);

  stroke(0);
  strokeWeight(1);

  //hair
  fill(currHairColor);
  ellipse(880, 360, 30);
  //shirt
  fill(currShirtColor);
  ellipse(880, 420, 30);


 //title
  textFont(myFont);
  textSize(40);
  fill(currShirtColor);

  //title
  stroke(0);
  strokeWeight(3);
  text('Create your own character', 380, 135);

  //DRAWING THE CHARACTER
  //t-shirt
  noStroke();
  fill(currShirtColor)
  rect(90, 530, 220, 100, 90, 90, 90, 90);

  //neck
    fill(currSkinColor)
    rect(175, 500, 50, 80, 90, 90, 90, 90);

  //shadow overlay (basically a purple overlay with low alpha creating a shaded variant of the skincolor)
    fill(94, 18, 52, 30);
    rect(175, 500, 50, 80, 90, 90, 90, 90);

  //hair
    fill(currHairColor);
    rect(75, 85, 250, 150, 90, 90, 30, 30);

  //ears
    fill(currSkinColor);
    ellipse(110, 265, 80, 80);
    ellipse(290, 265, 80, 80);

  //ears shadow overlay
    fill(94, 18, 52, 30);
    ellipse(110, 265, 80, 80);
    ellipse(290, 265, 80, 80);

  //another overlay
    fill(94, 18, 52, 30);
    ellipse(102, 265, 30, 40);
    ellipse(298, 265, 30, 40);

  //head
    fill(currSkinColor);
    noStroke();
    strokeWeight(4);
    rectMode(CENTER);
    rect(200, 320, 200, 400, 70, 70, 90, 90);

  //hair components that are drawn on top of the head
    fill(currHairColor);
    ellipse(150, 120, 100, 50);
    ellipse(250, 120, 100, 50);

  //eyebrows
    noStroke();
    fill(currHairColor);
    rect(140, 175, 70, 25, 90, 90, 90, 90);
    rect(260, 175, 70, 25, 90, 90, 90, 90);

  //nose (same kind of overlay as ears)
    fill(94, 18, 52, 30);
    rect(200, 280, 30, 70, 90, 90, 90, 90);

  //eyes
    fill(currEyeColor);
    rectMode(CENTER);
    rect(260, 220, 40, 40, 90, 90, 5, 5);
    rect(140, 220, 40, 40, 90, 90, 5, 5);

  //pupils
    fill(0, 0, 0, 80);
    ellipse(144, 216, 30, 30);
    ellipse(264, 216, 30, 30);

  //white reflections in eyes
    fill(230);
    ellipse(150, 210, 10, 8);
    ellipse(270, 210, 10, 8);

  //rosy cheeks
    fill(194, 18, 52, 30);
    ellipse(130, 295, 50, 50);

    ellipse(270, 295, 50, 50);

  //mouth
    fill(94, 18, 52, 95);
    arc(200, 370, 130, 100, 0, PI);

//for some reason this had to stay here for the code to work, lol idk
    rectMode(CORNER);
}

//below are functions that are called upon clicking the various buttons
function newName(){
  //basically I just moved the randomizations into the button click functions
  fill(230);
  rect(420, 220, 200, 30);
  textFont('Helvetica');
  fill('#000000');
  noStroke();
  textSize(14);
  text('Name: ' + (random(charName)), 430, 240);
}

function newAge()
{
  fill(230);
  rect(420, 260, 200, 30);
  textFont('Helvetica');
  fill('#000000');
  noStroke();
  textSize(14);
  text('Age: ' + floor(random(15, 56)), 430, 280);
}

function newPersonality(){
  fill(230);
  rect(420, 300, 300, 45);
  textFont('Helvetica');
  fill('#000000');
  noStroke();
  textSize(14);
  text('He is a' + (random(charTrait)) + (random(charNoun)), 430, 320);
}

function newInterest(){
  fill(230);
  rect(420, 340, 290, 45);
  textFont('Helvetica');
  fill('#000000');
  noStroke();
  textSize(14);
  text('He likes ' + (random(charLikes)), 430, 360);
}

function newDisinterest(){
  fill(230);
  rect(420, 380, 290, 45);
  textFont('Helvetica');
  fill('#000000');
  noStroke();
  textSize(14);
  text('He hates ' + (random(charDislikes)), 430, 400);
}

function newFunFact(){
  fill(230);
  rect(420, 420, 290, 60);
  textFont('Helvetica');
  fill('#000000');
  noStroke();
  textSize(14);
  text('Fun fact: ' + (random(charFact)), 430, 440);
}

function randomizeLooks()
{
  //all slider positions are basically re-randomized
  skincSlider.value(random(0, skinColor.length -1));
  eyecSlider.value(random(0, eyeColor.length -1));
  haircSlider.value(random(0, hairColor.length -1));
  shirtcSlider.value(random(0, shirtColor.length -1));
}

//function that reloads window when button is pressed
function randomizeAll()
{
  //calls all trait functions to randomize everything
  newName();
  newAge();
  newPersonality();
  newInterest();
  newDisinterest();
  newFunFact();
}
